Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: QIIME 2
Source: https://github.com/qiime2/q2-phylogeny/releases

Files: *
Copyright: 2016-2023 QIIME 2 development team
License: BSD-3-clause

Files: versioneer.py
Copyright: Brian Warner
License: CC0-1.0
Comment: Versioneer.py can be found here https://pypi.python.org/pypi/versioneer/
 and the corresponding github repository's readme states CC0-1.0

Files: debian/*
Copyright: 2021 Andreas Tille <tille@debian.org>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  .
  1.  Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
  .
  2.  Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
  .
  3.  Neither the name of the copyright holder nor the names of its
      contributors may be used to endorse or promote products derived from this
      software without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 "Public Domain Dedication" license:
 .
 The person who associated a work with this deed has dedicated the work
 to the public domain by waiving all of his or her rights to the work
 worldwide under copyright law, including all related and neighboring
 rights, to the extent allowed by law.
 .
 You can copy, modify, distribute and perform the work, even for commercial
 purposes, all without asking permission. See Other Information below.
